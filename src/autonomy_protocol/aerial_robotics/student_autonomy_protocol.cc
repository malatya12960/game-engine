#include "student_autonomy_protocol.h"

namespace game_engine {

  std::unordered_map<std::string, Trajectory>
  StudentAutonomyProtocol::UpdateTrajectories() {

    // STUDENTS: Fill out this function.  To make your code manageable, you are
    // encouraged to add whatever data members and member functions you find
    // convenient to the StudentAutonomyProtocol class. You can also add
    // non-member functions to this file.  In fact, feel free to create your own
    // classes in separate files. (You'll need to include the name of the *.cc
    // file for each class you create in the list of SOURCE_FILES in
    // ../CMakeLists.txt.)
    
    return std::unordered_map<std::string, Trajectory>();
  }
}
